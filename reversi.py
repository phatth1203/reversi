# reversi game
import os

# define WIDTH and HEIGHT
WIDTH = 8
HEIGHT = 8


def drawBoard(board):
    # draw the board on console
    print('  a b c d e f g h')
    for y in range(HEIGHT):
        print('%s ' % (y+1), end='')
        for x in range(WIDTH - 1):
            print(board[x][y] + ' ', end='')
        print(board[WIDTH - 1][y])


def getNewBoard():
    # Create new board
    board = []
    for i in range(WIDTH):
        board.append(['.', '.', '.', '.', '.', '.', '.', '.'])
    board[3][3] = 'W'
    board[3][4] = 'B'
    board[4][3] = 'B'
    board[4][4] = 'W'
    # return [
    # a   b   c   d   e   f   g   h
    # ['.','.','.','W','B','.','.','.'],
    # ['B','.','W','B','B','B','.','.'],
    # ['W','W','W','B','B','B','B','.'],
    # ['.','W','B','B','B','B','.','.'],
    # ['W','B','W','W','W','B','.','.'],
    # ['W','B','B','W','W','B','W','.'],
    # ['W','B','.','W','B','.','.','.'],
    # ['.','.','.','B','.','.','.','.']
    # ]
    return board


def makeMove(board, tile, xstart, ystart):
    #  make move for xstart and ystart
    flip_list = isValidMove(board, tile, xstart, ystart)
    if not flip_list:
        return False
    board[xstart][ystart] = tile
    for x, y in flip_list:
        board[x][y] = tile
    return True


def getValidMoves(arr):
    #  Return a new list with periods making
    # the valid moves the player can make.
    a = {1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g', 8: 'h'}
    listValidMoves = []
    if arr == []:
        return 'End'
    else:
        for x in range(len(arr)):
            if arr[x][0] in a.keys():
                listValidMoves.append(a[arr[x][0]] + str(arr[x][1]))
        return listValidMoves


def getScoreOfBoard(board):
    #  Determine the score by counting
    # the tiles. Return a dictionary with keys 'B' and 'W'
    bscore = 0
    wscore = 0
    for x in range(WIDTH):
        for y in range(HEIGHT):
            if board[x][y] == 'B':
                bscore += 1
            if board[x][y] == 'W':
                wscore += 1
    # xscore as Black, oscore as White
    return (bscore, wscore)


def string2int(string):
    dic = {
        'a': 0,
        'b': 1,
        'c': 2,
        'd': 3,
        'e': 4,
        'f': 5,
        'g': 6,
        'h': 7
    }
    return dic[string]


def int2string(num):
    dic = {
        0: 'a',
        1: 'b',
        2: 'c',
        3: 'd',
        4: 'e',
        5: 'f',
        6: 'g',
        7: 'h'
    }
    return dic[num]


def getPlayerMove(cell, move):
    if len(move) != 2:
        return False
    x = move[0]
    y = move[1]
    if y in "abcdefghijklmnopqrstuvwxyz":
        return False
    if(x in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'] and
       int(y) in range(1, 9, 1)):
        x = string2int(move[0])
        return ([int(x), int(y)-1])
    else:
        return False


def isInBoard(x, y):
    return x < WIDTH and x >= 0 and y < HEIGHT and y >= 0


def re_cell(cell):
    return 'W' if cell == 'B' else 'B'


def isValidMove(board, cell, x_move, y_move):
    if board[x_move][y_move] != '.' or not isInBoard(x_move, y_move):
        return False
    other_cell = re_cell(cell)
    flip_list = []
    ls = [[0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1]]
    for x_vector, y_vector in ls:
            # x    x    x
            # x   cell  x
            # x    x    x
        x, y = x_move, y_move
        x += x_vector
        y += y_vector
        while isInBoard(x, y) and board[x][y] == other_cell:
            # keep move
            x += x_vector
            y += y_vector
            if isInBoard(x, y) and board[x][y] == cell:
                while True:
                    x -= x_vector
                    y -= y_vector
                    if x == x_move and y == y_move:
                        break
                    flip_list.append([x, y])
    # if nothing in list
    if len(flip_list) == 0:
        return False
    return flip_list


def getMoveList(board, cell):
    move_list = []
    for x_move in range(WIDTH):
        for y_move in range(HEIGHT):
            if isValidMove(board, cell, x_move, y_move) is not False:
                # li = isValidMove(board,cell,x_move,y_move)
                # for xx,yy in li:8
                #     board[xx][yy] = 'x'
                move_list.append([x_move+1, y_move+1])
    return move_list


def gamePlay(cell1):
    board = getNewBoard()
    gameEnded = False
    cell = cell1
    while not gameEnded:
        # os.system('clear')
        cell = re_cell(cell)
        drawBoard(board)
        ValidMove = getValidMoves(getMoveList(board, cell))
        if ValidMove == 'End':
            bscore, wscore = getScoreOfBoard(board)
            print("Player %s cannot play." % (cell))
            drawBoard(board)
            print("Player %s cannot play." % (re_cell(cell)))
            print("End of the game. W: %s, B: %s" % (wscore, bscore))
            if bscore > wscore:
                print("B wins.")
            elif bscore < wscore:
                print("W wins.")
            else:
                print("Tie.")
            gameEnded = True
            break
        print("Valid choices: ", end='')
        for i in range(len(ValidMove)-1):
            print(ValidMove[i], end=' ')
        print(ValidMove[len(ValidMove)-1])
        while True:
            move = input("Player %s: " % (cell))
            playerMove = getPlayerMove(cell, move)
            if not playerMove:
                print("%s: Invalid choice" % (move))
                print("Valid choices: ", end='')
                for i in range(len(ValidMove)-1):
                    print(ValidMove[i], end=' ')
                print(ValidMove[len(ValidMove)-1])
                exit()
            else:
                if makeMove(board, cell, playerMove[0], playerMove[1]):
                    break
                else:
                    print("%s: Invalid choice" % (move))
                    print("Valid choices: ", end='')
                    for i in range(len(ValidMove)-1):
                        print(ValidMove[i], end=' ')
                    print(ValidMove[len(ValidMove)-1])
                    exit()


gamePlay('W')

# while True:
#         gamePlay('W')
#         choice = input("Do you want to play again?[y]/[n]: ")
#         if choice.lower() == 'n':
#             break
